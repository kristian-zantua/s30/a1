const express = require(`express`);
const mongoose = require(`mongoose`);
const dotenv = require(`dotenv`);
dotenv.config();
const app = express();
const PORT = 4006;

//Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Mongoose connection
    //mongoose.connect(<connection string>, {options})
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})    

//DB connection notification
    //use connection property of mongoose
const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`)) 


//SCHEMA

//Create a Schema for tasks
    //schema determines the structure of the documents to be written in the database
    //schema acts as a blueprint to our data
const userSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: [true, `Name is required`]
        },
        password: {
            type: String,
            required:[true, "Password is required"]  
        }
    }
)

//create a mode out of the schema
    //syntax: mongoose.model(<name of the model>,<schema where model came from>)
const User = mongoose.model(`User`, userSchema)
    //model is a programming interface that enables us to query and manipulate database using its methods


app.post(`/signup`, (req, res)=>{
    User.findOne({username: req.body.username})
	.then(result => {
		console.log(result)

		if(result !== null && result.username == req.body.username){
			return res.send("Duplicate User Found")
		}
		else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
			
			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
				
		}
	})



app.listen(PORT, ()=> console.log(`Server connected at port ${PORT}`))